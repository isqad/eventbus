package eventbus

import (
	"github.com/streadway/amqp"
	"os"
)

const (
	EXCHANGE_NAME          = "sysmsgs"
	ENV_KEY_AMQP_CONN_SPEC = "EVENTBUS_AMQP_CONNECT_SPEC"
)

type eventBusChannel struct {
	connection *amqp.Connection
	channel    *amqp.Channel
}

func newEventBusChannel() (*eventBusChannel, error) {
	eventBusChannel := &eventBusChannel{}
	conn, err := amqp.Dial(os.Getenv(ENV_KEY_AMQP_CONN_SPEC))
	if err != nil {
		return nil, err
	}
	eventBusChannel.connection = conn

	ch, err := eventBusChannel.connection.Channel()
	if err != nil {
		eventBusChannel.connection.Close()

		return nil, err
	}

	eventBusChannel.channel = ch

	err = eventBusChannel.channel.ExchangeDeclare(
		EXCHANGE_NAME, // name
		"fanout",      // type
		true,          // durable
		false,         // auto-deleted
		false,         // internal
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		eventBusChannel.channel.Close()
		eventBusChannel.connection.Close()

		return nil, err
	}

	return eventBusChannel, nil
}

func (c *eventBusChannel) pub(msg string) error {
	err := c.channel.Publish(
		EXCHANGE_NAME,
		"",
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(msg),
		},
	)
	if err != nil {
		return err
	}

	return nil
}

func (c *eventBusChannel) cunsume() (<-chan amqp.Delivery, error) {
	q, err := c.channel.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   //args
	)
	if err != nil {
		c.close()

		return nil, err
	}

	err = c.channel.QueueBind(
		q.Name,        // queue name
		"",            // routing key
		EXCHANGE_NAME, // exchange
		false,
		nil,
	)
	if err != nil {
		c.close()

		return nil, err
	}
	msgs, err := c.channel.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	if err != nil {
		c.close()

		return nil, err
	}

	return msgs, nil
}

func (c *eventBusChannel) close() {
	c.channel.Close()
	c.connection.Close()
}
