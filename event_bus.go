// System Bus
package eventbus

type EventBus struct {
	Messages chan []byte

	Consumed chan bool

	done    chan bool
	channel *eventBusChannel
}

type EventMessage struct {
	Type    string      `json:"type"`
	UserId  int64       `json:"user_id"`
	Payload interface{} `json:"payload"`
}

func NewEventBus() (*EventBus, error) {
	eventBus := &EventBus{Messages: make(chan []byte, 256), done: make(chan bool), Consumed: make(chan bool)}

	ch, err := newEventBusChannel()
	if err != nil {
		return nil, err
	}
	eventBus.channel = ch

	return eventBus, nil
}

func (b *EventBus) Stop() {
	b.done <- true
}

func (b *EventBus) Pub(message string) error {
	err := b.channel.pub(message)
	if err != nil {
		return err
	}

	return nil
}

func (b *EventBus) Sub() error {
	msgs, err := b.channel.cunsume()
	if err != nil {
		return err
	}
	close(b.Consumed)

	for {
		select {
		case d := <-msgs:
			b.Messages <- d.Body
		case <-b.done:

			close(b.Messages)
			b.channel.close()

			return nil
		}
	}
}
