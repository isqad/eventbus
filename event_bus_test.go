package eventbus

import (
	"fmt"
	"sync"
	"testing"
)

func TestNewEventBus(t *testing.T) {
	eventBus, err := NewEventBus()

	if err != nil {
		t.Errorf("NewEventBus() returns error: %v, want no errors", err)
	}

	if fmt.Sprintf("%T", eventBus) != "*eventbus.EventBus" {
		t.Errorf("NewEventBus() returns wrong type: %T, want *eventbus.EventBus", eventBus)
	}
}

func TestPub(t *testing.T) {
	eventBus, err := NewEventBus()

	if err != nil {
		t.Errorf("NewEventBus() returns error: %v, want no errors", err)
	}

	err = eventBus.Pub("Hello")
	if err != nil {
		t.Errorf("Pub() returns error: %v, want no errors", err)
	}
}

func TestSub(t *testing.T) {
	var (
		wg   sync.WaitGroup
		done = make(chan bool)
	)
	eventBus, err := NewEventBus()

	if err != nil {
		t.Errorf("NewEventBus() returns error: %v, want no errors", err)
	}

	wg.Add(3)

	go func(w *sync.WaitGroup) {
		eventBus.Sub()
		wg.Done()
	}(&wg)

	go func(w *sync.WaitGroup) {
		<-eventBus.Consumed

		done <- true

		msg := <-eventBus.Messages

		if string(msg[:]) != "test" {
			t.Errorf("expected test message, got: %s", string(msg[:]))
		}

		eventBus.Stop()

		wg.Done()
	}(&wg)

	go func(w *sync.WaitGroup) {
		<-done

		eventBus.Pub("test")

		wg.Done()
	}(&wg)

	wg.Wait()
}
