package main

import (
	"gitlab.com/isqad/eventbus"
	"log"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(1)

	eventBus, err := eventbus.NewEventBus()
	if err != nil {
		log.Fatal(err)
	}

	go func(wg *sync.WaitGroup) {
		eventBus.Sub()
		<-eventBus.Consumed

		for msg := range eventBus.Messages {
			log.Println("[x] Received: " + string(msg[:]))
		}

		wg.Done()
	}(&wg)

	time.Sleep(1 * time.Second)

	eventBus.Pub("Hello, world!")
	eventBus.Pub("foo bar")

	time.Sleep(1 * time.Second)

	eventBus.Stop()

	wg.Wait()
}
